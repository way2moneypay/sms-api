'use strict'
const express = require('express')
const app = express()
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const compression = require('compression')
// const requestIp = require('request-ip');
const config = require('config')
const helmet = require('helmet')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const timeout = require('connect-timeout')
const sanitize = require('mongo-sanitize')
const path = require('path')
var processexit = false

// var ResponseUtils = require("./restapp/utils/ResponseUtils");

const options = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 25, // Maintain up to 25 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: require('bluebird'),
  useNewUrlParser: true
}

const db = mongoose.connection

db.once('error', function (err) {
  console.error('mongoose connection error' + err)
  mongoose.disconnect()
})
db.on('open', function () {
  console.log('successfully connected to mongoose')
})
db.on('reconnected', function () {
  console.log('MongoDB reconnected!')
})
db.on('disconnected', function () {
  console.log('MongoDB disconnected!')
  if (!processexit) {
    mongoose.connect(config.mongodb.uri, options)
      .then(() => console.log('connection succesful'))
      .catch((err) => console.error(err))
  }
})

// mongoose.connect(config.mongodb.uri, options);

mongoose.connect(config.mongodb.uri, options)
  .then(() => console.log('connection succesful'))
  .catch((err) => console.error(err))

config.home_directory = __dirname
// const cache_options = {
//   dotfiles: 'ignore',
//   etag: true,
//   maxAge: '2d',
//   extensions: ['js', 'css', 'jpg', 'png', 'jpeg', 'html'],
//   index: false,
//   redirect: false,
//   expires: new Date(Date.now() + 86400000 + 86400000),
//   setHeaders: function (res, path, stat) {
//     // res.set('x-timestamp', Date.now())
//   }
// }

if (process.env.NODE_ENV !== 'development') {
  console.log('adding compression')
  // compression
  app.use(compression())
  // require('geoip-lite/scripts/updatedb.js');
} else {
  mongoose.set('debug', true)
}


app.use(timeout('60s'))
app.use(helmet())
// cookie parser
app.use(cookieParser())
// request ip middle ware
// app.use(requestIp.mw());
// app.use(mongoSanitize({
//     replaceWith: '_'
// }))

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()) // parse application/json
app.use(bodyParser.json({
  type: 'application/vnd.api+json'
})) // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({
  extended: true
})) // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')) // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
// if (process.env.NODE_ENV !== 'development') {
//   console.log('adding cache options');
//   app.use(serveStatic(__dirname + '/dist/browser', cache_options)); // set the static files location /public/img will be /img for users
//   app.use(serveStatic(__dirname + '/uploads', cache_options));
// } else {
//   // console.log(__dirname + '/src/assets');
//   app.use(serveStatic(__dirname + '/dist/browser')); // set the static files location /src/assets will be /img for users
//   app.use(serveStatic(__dirname + '/uploads'));
// }

// app.use(favicon(__dirname + '/src/favicon.ico'));
// app.use(robots(__dirname + '/robots.txt'));

app.use(cors(), function (req, res, next) {
  // res.header("Access-Control-Allow-Credentials", true);
  // res.header("Access-Control-Allow-Origin", '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type,Accept')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
  next()
})

const sanitizeCustom = function (req, res, next) {
  let bodykeys = Object.keys(req.body)
  bodykeys.forEach(function (keyObj) {
    req.body.keyObj = sanitize(req.body[keyObj])
  })
  bodykeys = Object.keys(req.query)
  bodykeys.forEach(function (keyObj) {
    req.query.keyObj = sanitize(req.query[keyObj])
  })
  bodykeys = Object.keys(req.params)
  bodykeys.forEach(function (keyObj) {
    req.params.keyObj = sanitize(req.params[keyObj])
  })
  next()
}

app.use(sanitizeCustom)

// routes ==================================================
require('./restapp/Routes')(app)

app.get('*', function (req, res, next) {
  if (app.get('env') === 'production') {
    res.send('welcome to ' + config.website)
  }
})

const logErrors = function (err, req, res, next) {
  console.log('in logErrors')
  console.error(err.stack)
  let NotFoundPage = path.join(__dirname, './restapp/utils/NotFoundPage.html')
  return res.sendFile(NotFoundPage)
  // next(err)
}

const clientErrorHandler = function (err, req, res, next) {
  console.log('in clientErrorHandler')
  if (req.xhr) {
    res.status(500).send({
      error: 'Something failed!'
    })
  } else {
    next(err)
  }
}

// app.use(function (req, res, next) {
//   var err = null
//   try {
//     decodeURIComponent(req.path)
//   } catch (e) {
//     err = e
//   }
//   if (err) {
//     console.log(err, req.url)
//     let NotFoundPage = path.join(__dirname, './restapp/utils/NotFoundPage.html')
//     return res.sendFile(NotFoundPage)
//   }
//   next()
// })

app.use(logErrors)
app.use(clientErrorHandler)

app.internalError = function (err, code, res) {
  const error = {}
  error.message = err
  error.status = 'error'
  res.statusCode = code
  return res.send(error)
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.send({
      message: err.message,
      error: err
    })
  })
} else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.send({
      message: err.message,
      error: {}
    })
  })
}

app.use(function (err, req, res, next) {
  res.end(err.message) // this catches the error!!
})

const haltOnTimedout = function (req, res, next) {
  if (!req.timedout) next()
}

app.use(haltOnTimedout)

/**
 * Normalize a port into a number, string, or false.
 */

const normalizePort = function (val) {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

const port = normalizePort(config.port || '8722')
app.set('port', port)
console.log('port running on :: ', config.port)
app.listen(port, config.host)

process.on('uncaughtException', function (uncaughtException) {
  console.error((new Date()).toUTCString() + ' uncaughtException:', uncaughtException.message)
  console.error(uncaughtException)
})

process.on('unhandledRejection', function (unhandledError) {
  console.error((new Date()).toUTCString() + ' unhandledRejection:', unhandledError.message)
  console.error(unhandledError)
})

process.on('SIGINT', function () {
  console.log(' on exit called by node')
  processexit = true
  mongoose.connection.close(function (err, resp) {
    if (err) {
      console.error(err)
    }
    process.exit(1)
  })
})

// console.log('Magic happens on port ' + port); // shoutout to the user
exports = module.exports = app // expose app
