var mongoose = require('mongoose')

var Schema = mongoose.Schema
var smscounts = new Schema({
  sent_count: {
    type: Number,
    default: 0
  },
  created_at: {
    type: String
  },
}, {
  versionKey: false
})

module.exports = mongoose.model('way2sms_counts', smscounts)
