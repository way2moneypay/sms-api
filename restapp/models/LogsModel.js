var mongoose = require('mongoose')

var Schema = mongoose.Schema
var logs = new Schema({
  message: {
    type: String
  },
  type: {
    type: String
  },
  code: {
    type: String
  },
  error_data: {
    type: String
  },
  error_info: {
    type: String
  },
  file_name: {
    type: String
  },
  function_name: {
    type: String
  },
  date: {
    type: Number,
    default: Date.now
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('logs', logs)
