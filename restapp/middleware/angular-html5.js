'use strict'

module.exports = function (params) {
  return function angularHtml5 (req, res, next) {
    if (req.path.indexOf('/api/') === 0) {
      next()
    } else {
      res.sendFile('./dist/index.html')
    }
  }
}
