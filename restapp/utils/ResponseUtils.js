var ResponseUtils = module.exports

var LogsModel = require('../models/LogsModel')
// var thisFileName = 'restapp/utils/ResponseUtils.js'

ResponseUtils.Print = function (fileName, functionName) {
  let errorMessage = 'Error occured at ' + fileName + ' in ' + functionName + ' : '
  let logMessage = 'Logging for ' + fileName + ' in ' + functionName + ' : '
  let exceptionMessage = 'Exception occured at ' + fileName + ' in ' + functionName + ' : '

  this.error = function (message) {
    console.error(errorMessage)
    let errorObject = {}
    errorObject.type = 'ERROR'
    errorObject.error_message = message
    errorObject.file_name = fileName
    errorObject.function_name = functionName
    console.error(errorObject)
  }

  this.log = function (message) {
    console.log(logMessage)
    let logObject = {}
    logObject.type = 'LOGGING'
    logObject.log_message = message
    logObject.file_name = fileName
    logObject.function_name = functionName
    console.log(logObject)
  }

  this.exception = function (message) {
    console.log(exceptionMessage)
    let exceptionObject = {}
    exceptionObject.type = 'EXCEPTION'
    exceptionObject.log_message = message
    exceptionObject.file_name = fileName
    exceptionObject.function_name = functionName
    console.log(exceptionObject)
  }
}

var saveLogsData = function (data, code, message, type, fileName, functionName) {
  let logsData = new LogsModel()

  logsData.message = message
  logsData.type = type
  logsData.code = code
  logsData.error_data = data
  logsData.file_name = fileName
  logsData.function_name = functionName

  logsData.save(function (logsStoredError, logsStored) {
    if (logsStoredError) {
      console.error('error while saving in ' + type, logsData)
    };
  })
}

ResponseUtils.Logs = function (fileName, functionName) {
  this.error = function (data, code, message) {
    saveLogsData(data, code, message, 'ERROR', fileName, functionName)
  }

  this.log = function (data, code, message) {
    saveLogsData(data, code, message, 'LOG', fileName, functionName)
  }

  this.exception = function (data, code, message) {
    saveLogsData(data, code, message, 'EXCEPTION', fileName, functionName)
  }
}

ResponseUtils.PrintCommon = function () {
  // let error_message = "Error occured at " + file_name + " in " + function_name + " : ";
  // let log_message = "Logging for " + file_name + " in " + function_name + " : ";
  // let exception_message = "Exception occured at " + file_name + " in " + function_name + " : ";

  this.error = function (message, totalError) {
    let errorObject = {}
    errorObject.type = 'ERROR'
    errorObject.error_message = message
    errorObject.error_info = getErrorInfo(totalError)
    console.error(errorObject)
  }

  this.log = function (message, totalError) {
    // console.log(log_message);
    let logObject = {}
    logObject.type = 'LOGGING'
    logObject.log_message = message
    logObject.log_info = getErrorInfo(totalError)

    console.log(logObject)
  }

  this.exception = function (message, totalError) {
    // console.log(exception_message);
    let exceptionObject = {}
    exceptionObject.type = 'EXCEPTION'
    exceptionObject.log_message = message
    exceptionObject.exception_info = getErrorInfo(totalError)

    console.log(exceptionObject)
  }
}

ResponseUtils.Response = function (apiResponse) {
  let errorObject = {}
  errorObject.status_code = 400
  errorObject.status = 'ERR'
  errorObject.type = 'ERROR'

  let successObject = {}
  successObject.status_code = 200
  successObject.status = 'SUCCESS'
  successObject.type = 'SUCCESS'

  this.error = function (message, status, statusCode, data) {
    errorObject.message = message
    errorObject.status = status
    errorObject.status_code = statusCode || errorObject.statusCode
    if (data || data === 0) {
      errorObject.data = data
    } else {
      errorObject.data = {}
    }
    // (data || data === 0) ?  : 'no data'

    apiResponse.status(200).send(errorObject)
  }

  this.success = function (message, data, status) {
    successObject.message = message
    successObject.status = status || successObject.status
    if (data || data === 0) {
      successObject.data = data
    } else {
      successObject.data = {}
    }
    // data || data == 0 ? .data = data : 'no data'

    apiResponse.status(200).send(successObject)
  }

  this.errorData = function (message, status, statusCode) {
    errorObject.message = message
    errorObject.status = status
    errorObject.status_code = statusCode || errorObject.status_code

    return errorObject
  }

  this.successData = function (message, data, status) {
    successObject.message = message
    successObject.status = status || successObject.status
    if (data || data === 0) {
      successObject.data = data
    } else {
      successObject.data = {}
    }
    // data || date == 0 ? success_object.data = data : 'no data'

    return successObject
  }
}

var getErrorInfo = function (error) {
  // console.log("error_data :::::::::::::::::::::::::::::: ", error.stack.split("\n")[1].trim());
  if (error.stack.split('\n')[1]) {
    return error.stack.split('\n')[1].trim()
  } else {
    return error
  }
}

var saveLogsDataWithInfo = function (message, code, data, type, error, totalError) {
  let logsData = new LogsModel()

  logsData.message = message
  logsData.type = type
  logsData.code = code
  logsData.error_data = data
  logsData.error_info = totalError ? error.stack : getErrorInfo(error)

  logsData.save(function (logsStoreError, logsStored) {
    if (logsStoreError) {
      console.error(logsStoreError)
      console.error('error while saving in ' + type, logsData)
    };
  })
}

ResponseUtils.LogsCommon = function () {
  this.error = function (message, code, data, error, totalError) {
    saveLogsDataWithInfo(message, code, data, 'ERROR', error, totalError)
  }

  this.log = function (message, code, data, error, totalError) {
    saveLogsDataWithInfo(message, code, data, 'LOG', error)
  }

  this.exception = function (message, code, data, error, totalError) {
    saveLogsDataWithInfo(message, code, data, 'EXCEPTION', error, totalError)
  }
}
