// UTILS AND SERVICES AND CONFIG

const ResponseUtils = require('./ResponseUtils')

const SMSAlertsModel = require('./../models/SMSAlertsModel')
const EmailAlertsModel = require('./../models/EmailAlertsModel')
const EncDec = require('./EncDec')

// NODE MODULES

const AppConfig = require('config')
const MYSQL = require('mysql')
const Request = require('request')
const replaceall = require('replaceall')

// INTIALIZATIONS

var NotificationUtils = module.exports

var thisFileName = 'restapp/utils/NotificationUtils'

NotificationUtils.notification = function () {

}

NotificationUtils.sms = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.sms'
  let Print = ResponseUtils.print(thisFileName, thisFunctionName)

  console.log('*******TEmplate Name at NotificationUtils.sms***************', templateName, configuration)
  try {
    SMSAlertsModel.findOne({
      alert_type: templateName,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting sms template with name', templateName)
        callback(err)
      } else if (resp && configuration.mobile && configuration.mobile.length === 10) {
        let body = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          body = body.replace('##' + obj + '##', configuration.replace[obj])
        })
        let con = MYSQL.createConnection({
          host: AppConfig.smsdb.hostname,
          user: AppConfig.smsdb.username,
          password: AppConfig.smsdb.password,
          database: AppConfig.smsdb.database
        })
        con.connect(function (err) {
          if (err) {
            console.log('__==RECHED TO FIRST ERR==___')
            console.error(err)
          }
          console.log('configuration :: ', configuration)
          let senderId = configuration.sender_id ? configuration.sender_id : resp.sender_id

          let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
          con.query(sql, [configuration.mobile, body, senderId, senderId], function (err, result) {
            con.end()
            if (err) {
              console.log('__==RECHED TO SECOND ERR==___')
              console.error(err)
              return callback(err)
            }
            callback(null, 'sms sent successfully')
          })
        })
      } else {
        console.log(resp)
        console.log('invalid mobile number length')
        let callbackError = 'invalid mobile number'
        callback(callbackError)
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}

NotificationUtils.email = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.email'
  let Print = ResponseUtils.print(thisFileName, thisFunctionName)

  try {
    EmailAlertsModel.findOne({
      alert_name: templateName,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', templateName)
        callback(err)
      } else {
        // console.log("resp, ", resp);
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          emailmsg.em = configuration.email
          let body = resp.alert_content
          Object.keys(configuration.replace).forEach(function (obj) {
            body = replaceall('##' + obj + '##', configuration.replace[obj], body)
          })
          emailmsg.body = body
          emailmsg.subj = resp.alert_subject
          emailmsg.fromname = 'Way2Money'
          emailmsg.msgid = Date.now()
          emailmsg.fromdomain = 'way2moneymail.com'
          let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')

          encemail = replaceall('/', '<->', encemail)
          Request(resp.api_url + encemail, function (error, response, body) {
            console.log('error:', error) // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode) // Print the response status code if a response was received
            console.log('body:', body) // Print the HTML for the Google homepage.
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          let callbackError = 'invalid alert route'
          callback(callbackError)
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
