const moment = require('moment')
const rn = require('random-number')
const randomstring = require('randomstring')
const MerchantModel = require('./../models/MerchantModel')
const PaymentGatewayModel = require('./../models/PaymentGatewayModel')

var dateFormat = require('dateformat')
const AppConfig = require('config')
var JWT = require('jsonwebtoken')

const GeneralUtils = module.exports

let timeZoneTypes = {
  'ist': 11 * 30 * 60 * 1000,
  'gmt': 0
}

GeneralUtils.unixTimeDiff = function (timeOne, timeTwo, diffType) {
  // diff_type in [years, months, weeks, days, hours, minutes, seconds]

  timeOne = moment(timeOne)
  timeTwo = moment(timeTwo)

  let timeDiff = timeTwo.diff(timeOne, diffType)
  return timeDiff
}

GeneralUtils.isUnixSameDate = function (timeOne, timeTwo, millesOne, millesTwo) {
  timeOne = timeOne / millesOne
  timeTwo = timeTwo / millesTwo

  timeOne = moment.unix(timeOne).format('MM/DD/YYYY')
  timeTwo = moment.unix(timeTwo).format('MM/DD/YYYY')
  return timeOne === timeTwo
}

GeneralUtils.randomNumber = function (minimum, maximum) {
  var options = {
    min: minimum,
    max: maximum,
    integer: true
  }

  return rn(options)
}

GeneralUtils.randomString = function (stringLength) {
  return randomstring.generate(stringLength)
}

GeneralUtils.generateRandomDataForSendingSms = function (type) {
  let randomData
  if (type === 'verify_mobile') {
    randomData = GeneralUtils.randomNumber(1000, 9999)
  } else if (type === 'send_password') {
    randomData = GeneralUtils.randomString(6)
  }
  return randomData
}

GeneralUtils.checkAllowForSendingOtpOrPassword = function (messageSendingType, currentMilliseconds, previousMilliseconds, messagesCount) {
  let returnData = {}
  returnData = {
    success: true,
    error_code: null,
    error_message: null
  }

  // let maximum_duration_between_sending_sms = 45; // 45 seconds
  let checkingConditions = {}

  // checking sms count and last updated date
  if (messageSendingType === 'verify_mobile') {
    checkingConditions.time = 25
    checkingConditions.count = 100
  } else if (messageSendingType === 'send_password') {
    checkingConditions.time = 25
    checkingConditions.count = 100
  }

  // checking time between sending messages
  if (GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(previousMilliseconds) !== GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(currentMilliseconds)) {
    returnData.error_message = 'Update count to 0'
    returnData.error_code = 'COUNTRESET'
    returnData.success = true
  } else if (GeneralUtils.getDifferenceBetweenMillisecondsInSeconds(previousMilliseconds, currentMilliseconds) < checkingConditions.time) {
    returnData.error_message = 'Maximum time between sending sms is 25 seconds. Please try again after sometime'
    returnData.error_code = 'TIMEEXCEED'
    returnData.success = false
    // checking two milliseconds are equal.. It convert current milliseconds to current day start milliseconds then compare two milliseconds
  } else if (GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(previousMilliseconds) === GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds(currentMilliseconds)) {
    // checking if user exhaust maximum count to send sms
    if (messagesCount > checkingConditions.count) {
      returnData.error_message = 'Maximum count for sending messages per day is ' + checkingConditions.count + '. Please try later.'
      returnData.error_code = 'EXCEEDSMSCOUNT'
      returnData.success = false
    }
  }

  return returnData
}

GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds = function (inputMilliseconds) {
  let temporaryDate = new Date(inputMilliseconds)
  let convertedDate = temporaryDate.getFullYear() + '-' + (temporaryDate.getMonth() + 1) + '-' + temporaryDate.getDate()
  return new Date(convertedDate).getTime()
}

GeneralUtils.getDifferenceBetweenMillisecondsInSeconds = function (previousMilliseconds, currentMilliseconds) {
  return ((currentMilliseconds - previousMilliseconds) / 1000)
}

GeneralUtils.copyFeilds = function (object, secondObject, copyFeilds) {
  secondObject = secondObject || {}

  let keys = copyFeilds || Object.keys(object)

  for (var index = 0; index < keys.length; ++index) {
    let key = keys[index]
    let value = object[key]
    secondObject[key] = value
  }

  return secondObject
}

GeneralUtils.copyFeildsWithAllTrue = function (object, secondObject, copyFeilds) {
  secondObject = secondObject || {}

  let keys = copyFeilds || Object.keys(object)

  for (var index = 0; index < keys.length; ++index) {
    let key = keys[index]
    let value = true
    secondObject[key] = value
  }

  return secondObject
}

// calculate estimated loan amout to give for leads based on loan type and etc.,
GeneralUtils.estimatedLoanAmount = function (loanData) {
  console.log('loan data new', loanData)
  if (loanData.loanType === 'PERSONAL') {
    // let M = (loan_data.loan_amount * 11 * (( 12 ** loan_data.loan_tenure) / (12 ** loan_data.loan_tenure) - 1));
    // let M = (loan_data.loan_amount * 11 * (( Math.pow(12, loan_data.loan_tenure)) / (Math.pow(12,loan_data.loan_tenure)) - 1));
    let M = loanData.loan_amount * 11 * ((12 ^ loanData.loan_tenure) / ((12 ^ loanData.loan_tenure) - 1))
    // let M = (loan_data.loan_amount * 11 * (( 12 * loan_data.loan_tenure) / (12*loan_data.loan_tenure) - 1));
    console.log('m value ', M)
    let X = (loanData.salary * 0.5 - loanData.current_emi) / (M * 100000 / loanData.loan_amount)
    console.log('x value ', X)
    return X
  }
  // else if (loanType === 'HOME') {

  // }
}

GeneralUtils.getIP = function (request) {
  var ip = request.headers['x-forwarded-for'] ||
    request.connection.remoteAddress ||
    request.socket.remoteAddress ||
    (request.connection.socket ? request.connection.socket.remoteAddress : null)

  return ip
}

// convert current date to specific timezone type (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-01T18:30:00.000Z if timezone is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZone = function (timeZoneType, inputDate) { // converts correct date to utc date
  let newDate = dateFormat(inputDate, 'isoUtcDateTime')
  console.log(newDate)
  newDate = new Date(new Date(newDate).getTime() + timeZoneTypes[timeZoneType]) // converting to indian standards
  return newDate
}

// convert current date to end of the day in specific time zone (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-02T18:29:59.999Z in case time zone type is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZoneEndOfTheDay = function (timeZoneType, inputDate) { // it converts correct date to last minute of day in utc format
  let newDate = dateFormat(inputDate, 'isoUtcDateTime')
  newDate = new Date(new Date(newDate).getTime() + timeZoneTypes[timeZoneType] + 86399999) // converting to indian standard time and make date to last second of the day
  return newDate
}

GeneralUtils.dateConversionFromCorrectToIst = function (inputDate) { // converts correct date to utc date
  let newDate = dateFormat(inputDate, 'isoUtcDateTime')
  newDate = new Date(new Date(newDate).getTime() + (11 * 30 * 60 * 1000)) // converting to indian standards
  return newDate
}

GeneralUtils.dateConversionFromCorrectToIstEndOfTheDay = function (inputDate) { // it converts correct date to last minute of day in utc format
  let newDate = dateFormat(inputDate, 'isoUtcDateTime')
  newDate = new Date(new Date(newDate).getTime() + (11 * 30 * 60 * 1000) + 86399999) // converting to indian standard time and make date to last second of the day
  return newDate
}

// Calculating FOIR percentage
GeneralUtils.calculateFOIR = function (salary, emi) {
  return ((emi / salary) * 100)
}

// Calculating Number years from given date
GeneralUtils.calculateYearsBetweenDate = function (date) {
  let newDate1 = moment(date, 'YYYY-MM-DD')
  let currentDate = dateFormat(new Date(), 'isoUtcDateTime')
  let newDate2 = moment(currentDate, 'YYYY-MM-DD')

  currentDate = null

  return (newDate2.diff(newDate1, 'years'))
}

GeneralUtils.calculateProcessingFee = function (loanAmount, loanName) {
  let newCalculatedProcessingFeeAmount = 0
  if (loanName === 'FULLERTON') {
    /* checking loan amount to make processing fess */
    if (loanAmount < 100000) { /* <1L */
      newCalculatedProcessingFeeAmount = (loanAmount * (4 / 100))
    } else if (loanAmount < 200000 && loanAmount >= 100000) { /* 1L-2L */
      newCalculatedProcessingFeeAmount = (loanAmount * (3.5 / 100))
    } else if (loanAmount < 300000 && loanAmount >= 200000) { /* 2L-3L */
      newCalculatedProcessingFeeAmount = (loanAmount * (3.5 / 100))
    } else if (loanAmount < 500000 && loanAmount >= 300000) { /* 3L-5L */
      newCalculatedProcessingFeeAmount = (loanAmount * (3.0 / 100))
    } else if (loanAmount >= 500000) { /* >5L */
      newCalculatedProcessingFeeAmount = (loanAmount * (2.5 / 100))
    }
  }

  return newCalculatedProcessingFeeAmount
}

/* Ramu */
/* Checking post fior */
GeneralUtils.postFior = function (leadDetails, partnerDetails) {
  /* calculating partner emi ConsumerLoanController2 */
  let partnerEmi = GeneralUtils.calculateEmi(leadDetails.loan_amount, leadDetails.loan_tenure, partnerDetails.interest_rate_min)
  let updatedEmi = leadDetails.current_emi + partnerEmi
  let newMonthlyIncome = leadDetails.occupation === 'SALARIED' ? leadDetails.monthlyIncome : (leadDetails.monthlyIncome / 12)
  let fiorPercentage = (updatedEmi / newMonthlyIncome)
  console.log('partner emi updated emi new monthly income ', partnerEmi, updatedEmi, newMonthlyIncome)
  if (fiorPercentage <= partnerDetails.min_borrow_requirements.fior) {
    return true
  } else {
    return false
  }
}

/* Ramu */
/* Calculates foir percentage for a lead */
GeneralUtils.calculateFoirPercentage = function (currentEmi, monthlyIncome) {
  currentEmi = currentEmi || 0
  return ((currentEmi * 100) / monthlyIncome)
}

/* Ramu */
/* Calculate emi for loan amount */
GeneralUtils.calculateEmi = function (loanAmount, loanTenure, interestRateMin) {
  let installments = loanTenure < 12 ? loanTenure : 12
  return GeneralUtils.getEmiAfterPreparingData(loanAmount, interestRateMin, installments, loanTenure)
}

/* Ramu */
/* getting emi from ptr formula */
GeneralUtils.getEmiAfterPreparingData = function (P, I, IN, N) {
  let R = I / IN
  R = R / 100
  let R1 = R + 1
  let part1 = Math.pow(R1, N)
  let part2 = Math.pow(R1, N) - 1
  let part3 = part1 / part2

  return (P * (R * part3)).toFixed(2)
}

/* Ramu */
/* Getting monthly based salaried and self employed */
/* for salaried monthly is monthly income */
/* for self employed monthly income(it is annual income field in our database) is monthly_income /12 */
GeneralUtils.getMonthlyIncome = function (monthlyIncome, occupation) {
  let newMonthlyIncome

  /* checking occupation type */
  if (occupation === 'SALARIED') {
    newMonthlyIncome = monthlyIncome
  } else {
    newMonthlyIncome = (monthlyIncome / 12)
  }

  return newMonthlyIncome
}

// Function for email verification
// check email validations
GeneralUtils.validateEmail = function (email) {
  if (!email) {
    return false
  }
  // const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,63})?$/;
  // const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
  return emailRegex.test(email)
}

GeneralUtils.decryptVerifyToken = function (token) {
  console.log('token called')
  let tokenData = JWT.verify(token, AppConfig.jwtsecret, function (tokenVerifyError, tokenData) {
    if (tokenVerifyError) {
      console.log('token error')
      return 'DECRYPTTOKENERR'
    } else {
      console.log('token data', tokenData)
      return tokenData
    }
  })
  return tokenData
}

GeneralUtils.storeDefaultMerchants = function () {
  console.log('in store default merchants')
  MerchantModel.findOneAndUpdate({
    'name': 'Web'
  }, {
    $set: {
      'email': 'web@way2money.com',
      'mobile': '9999999999',
      'company': 'Way2online',
      'logo_url': 'https://apk4free.net/wp-content/uploads/2017/05/Way2SMS-Free-SMS.png',
      'merchant_category': 'aLL',
      'brand_name': 'Way2Online',
      'password': 'sha1$11611300$1$fb2c5098b902535d4fef4f2f169932a2de164cd8',
      'register_date': Date.now(),
      'merchant_status': 1,
      'token_validity': 5,
      'pending_amt': -1000,
      'approved_amt': 500
    }
  }, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
  MerchantModel.findOneAndUpdate({
    'name': 'Mobile'
  }, {
    $set: {
      'email': 'mobile@way2money.com',
      'mobile': '8888888888',
      'company': 'Way2online',
      'logo_url': 'https://apk4free.net/wp-content/uploads/2017/05/Way2SMS-Free-SMS.png',
      'merchant_category': 'aLL',
      'brand_name': 'Way2Online',
      'password': 'sha1$11611300$1$fb2c5098b902535d4fef4f2f169932a2de164cd8',
      'register_date': Date.now(),
      'merchant_status': 1,
      'token_validity': 5,
      'pending_amt': -1000,
      'approved_amt': 500
    }
  }, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
  PaymentGatewayModel.findOneAndUpdate({
    gateway_name: 'InstaMojo'
  }, {}, {
    new: true,
    upsert: true
  }, function (err, resp) {
    if (err) {
      console.error(err)
    }
  })
}

GeneralUtils.getTimeStampDateFilter = function (fromDate, toDate, dateFormats, addDay) {
  if (!fromDate || !toDate) {
    return null
  }

  if (!moment(fromDate, dateFormats).isValid() || !moment(toDate, dateFormats).isValid()) {
    return null
  }

  console.log(fromDate, toDate)
  fromDate = (moment(fromDate, dateFormats) / 1)
  if (addDay !== false) toDate = moment(toDate, dateFormats).add(86399, 'seconds') / 1
  if (addDay === false) toDate = moment(toDate, dateFormats) / 1
  console.log('-------------------ADD DAY---------', addDay)
  return {
    $gte: fromDate,
    $lte: toDate
  }
}
