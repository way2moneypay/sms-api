'use strict'

var RequestUtils = module.exports

RequestUtils.MakeRequestOptions = function (url, method, data, headers) {
  let options = {}
  let paramData = data || {}
  options.url = url
  options.json = true
  options.method = method
  options.headers = headers || {
    'content-type': 'application/json'
  }
  if (method === 'get' || method === 'GET') {
    options.qs = paramData
  } else if (method === 'post' || method === 'POST') {
    options.body = paramData
  }
  return options
}

RequestUtils.MakeRequestOptionsWithType = function (url, method, data, bodyType, headers) {
  let options = {}
  let paramData = data || {}
  options.url = url
  options.json = true
  options.method = method
  options.headers = headers || {
    'content-type': 'application/json'
  }
  if (method === 'get' || method === 'GET') {
    options.qs = paramData
  } else if (method === 'post' || method === 'POST') {
    options[bodyType] = paramData
  }
  return options
}
