const AppConfig = require('config')
const ResponseUtils = require('./ResponseUtils')
const JWT = require('jsonwebtoken')

var thisFileName = 'restapp/utils/AuthenticateUtils'

var AuthenticateUtils = module.exports

AuthenticateUtils.userAuthentication = function (apiRequest, apiResponse, nextService) {
  let thisFunctionName = 'AuthenticateUtils.userAuthentication'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  let Response = new ResponseUtils.Response(apiResponse)

  // check header or url parameters or post parameters for token
  var authenticationToken = apiRequest.headers['x-access-token']
  // decode token
  if (authenticationToken) {
    // verifies secret and checks exp
    JWT.verify(authenticationToken, AppConfig.jwtsecret, function (tokenVerifyError, tokenData) {
      console.log(tokenVerifyError)
      console.log('TOKEN DATA IN AUTHENTICATION UTILS', tokenData)
      if (tokenVerifyError) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        // {
        //     // if everything is good, save to request for use in other routes
        //     const now = new Date().getTime();

        //     if(!token_data.a_expiry || token_data.a_expiry < now ){
        //         console.log('---------auth -------------',"verification authontication expired");
        //         const param = "Authenticaiton token expired.Please login again"
        //         // delete apiResponse.headers['x-access-token'];
        //         return Response.error(param,"AUTHTOKENERR",500);
        //     }else{
        // console.log(".....authontication failed");
        apiRequest.user = tokenData
        nextService()
      }
      // }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}

AuthenticateUtils.userAuthenticationWithQuery = function (apiRequest, apiResponse, nextService) {
  let thisFunctionName = 'AuthenticateUtils.userAuthenticationWithQuery'
  let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
  let Response = new ResponseUtils.Response(apiResponse)

  console.log('api_req BOdy :: ', apiRequest.body)

  // check header or url parameters or post parameters for token
  var authenticationToken = apiRequest.headers['x-access-token'] || apiRequest.query.token || apiRequest.body['x-access-token']
  // decode token
  if (authenticationToken) {
    // verifies secret and checks exp
    JWT.verify(authenticationToken, AppConfig.jwtsecret, function (tokenVerifyError, tokenData) {
      if (tokenVerifyError) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        console.log('TOKEN DATA AT AUTHENTICATION', tokenData)
        // if everything is good, save to request for use in other routes
        apiRequest.user = tokenData
        nextService()
      }
    })
  } else {
    Print.error('Authentication token not found in request.')
    return Response.error('User athentication failed.', 'ERR')
  }
}
