const ValidationUtils = module.exports

const moment = require('moment')

ValidationUtils.required = function (value, configuration) {
  let exceptZero = configuration.except_zero
  if ((!value && !exceptZero) || (exceptZero && !value && value !== 0)) return false
  return true
}

ValidationUtils.minimumValue = function (value, configuration) {
  let minimum = configuration['min']
  return value > minimum
}

ValidationUtils.maximumValue = function (value, configuration) {
  let maximum = configuration['max']
  return value < maximum
}

ValidationUtils.range = function (value, configuration) {
  let minimum = configuration['min']
  let maximum = configuration['max']

  return (value > minimum && value < maximum)
}

ValidationUtils.minimumLength = function (value, configuration) {
  let minimum = configuration['min']
  return value.length > minimum
}

ValidationUtils.maximumLength = function (value, configuration) {
  let maximum = configuration['max']
  return value.length < maximum
}

ValidationUtils.rangeLength = function (value, configuration) {
  let minimum = configuration['min']
  let maximum = configuration['max']

  return (value.length > minimum && value.length < maximum)
}

ValidationUtils.typeCheck = function (value, configuration) {
  let type = configuration['type']
  return (typeof value).toString() === type
}

ValidationUtils.valueCheck = function (value, configuration) {
  let checkData = configuration['value']
  return value === checkData
}

ValidationUtils.inList = function (value, configuration) {
  let list = configuration['list']
  return list.indexOf(value) !== -1
}

ValidationUtils.email = function (value) {
  let pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
  // let pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
  let regex = new RegExp(pattern, ['g'])
  return regex.test(value)
}

ValidationUtils.pattern = function (value, configuration) {
  let pattern = configuration['pattern']
  let regex = new RegExp(pattern)
  return regex.test(value)
}

ValidationUtils.date = function (value, configuration) {
  let formatsList = configuration['formats_list']
  return moment(value, formatsList, true).isValid()
}

ValidationUtils.dateDiff = function (value, configuration, totalData) {
  let formatsList = configuration['formats_list']
  let secondValueType = configuration['second_value_type']
  let secondValue = configuration['second_value']
  let minimumDifference = configuration['minimum_difference']
  let maximumDifference = configuration['maximum_differencee']
  let checkingType = configuration['cheking_type']
  let diffType = configuration['diff_type'] ? configuration['diff_type'] : 'years'
  let diffBetween = configuration['diff_between']

  // if (!ValidationUtils.date(value, formats_list)) {
  //   return false;
  // }

  console.log(value)
  let givenDate = moment(value, formatsList)
  let anotherDate = moment()

  if (secondValueType === 'field') {
    if (!totalData || !secondValue || !totalData[secondValue]) {
      return false
    }

    anotherDate = moment(totalData[secondValue])
  }

  console.log(configuration)
  // return true;

  let difference = !diffBetween ? anotherDate.diff(givenDate, diffType) : givenDate.diff(anotherDate, diffType)

  console.log(difference, checkingType)

  if (checkingType === 'max') {
    return difference < maximumDifference
  }

  if (checkingType === 'minimum') {
    return difference > minimumDifference
  }

  if (checkingType === 'range') {
    return (difference > minimumDifference && difference < maximumDifference)
  }
}

ValidationUtils.dependecyCheckOne = function (totalData, configuration, totalConfiguration) {
  let fieldName = configuration['field_name']
  let fieldValid = configuration['field_valid']
  let feildValidation = true
  let extraValidation = configuration['extra_validation']

  if (fieldValid) {
    feildValidation = ValidationUtils.validateFeild(totalData[fieldName], totalConfiguration[fieldName], totalConfiguration, totalData)
    if (feildValidation.error) {
      return false
    }
  }

  if (extraValidation) {
    let valid = ValidationUtils.validateFeild(totalData, extraValidation, totalConfiguration, totalData)
    return valid
  }

  return true
}

ValidationUtils.dependecyCheckMultiple = function (type, totalConfiguration, fieldsConfiguration, totalData) {
  for (var index = 0; index < fieldsConfiguration; ++index) {
    let configuration = fieldsConfiguration[index]
    let valid = ValidationUtils.dependecyCheckOne(totalData, configuration, totalConfiguration)

    if (type === 'or' && valid) {
      return true
    }

    if (type === 'and' && !valid) {
      return false
    }
  }

  if (type === 'or') {
    return false
  }

  if (type === 'and') {
    return true
  }
}

ValidationUtils.dependencyCheck = function (configuration, totalConfiguration, totalData) {
  for (var index = 0; index < configuration.length; ++index) {
    let type = configuration[index]['type']
    let fieldsConfiguration = configuration[index]['config']

    if (!ValidationUtils.dependecyCheckMultiple(type, totalConfiguration, fieldsConfiguration, totalData)) {
      return false
    }
  }

  return true
}

ValidationUtils.validateFeild = function (value, configuration, totalConfiguration, totalData) {
  if (configuration['dependendy']) {
    let dependencyValidations = configuration['dependendy']['validations']
    let valid = ValidationUtils.dependecyCheck(dependencyValidations, totalConfiguration, totalData)
    if (!valid) {
      return configuration['dependendy']['return_value']
    }
  }

  let feildValidations = configuration['validations']

  for (var index = 0; index < feildValidations.length; ++index) {
    let validation = feildValidations[index]
    let validationName = validation['name']
    let validationConfig = validation['config']

    if (!ValidationUtils[validationName](value, validationConfig, totalConfiguration)) {
      return {
        error: true,
        message: validationConfig['error_message']
      }
    }
  }

  return {
    error: false
  }
}

ValidationUtils.validate = function (totalData, configuration) {
  let returnType = configuration['return_type']
  let validationConf = configuration['configuration']
  let validationFeilds = Object.keys(validationConf)
  let returnData = {
    validation_error: false,
    data: []
  }

  for (var index = 0; index < validationFeilds.length; ++index) {
    let field = validationFeilds[index]
    let value = totalData[field]
    let fieldConf = validationConf[field]

    let validationData = ValidationUtils.validateFeild(value, fieldConf, validationConf, totalData)
    if (validationData.error) {
      returnData.validation_error = true
      returnData.data.push({
        key: field,
        message: validationData.message
      })

      if (returnType === 'single') {
        return returnData
      }
    }
  }

  return returnData
}
// Preparing configuration

ValidationUtils.Required = function (exceptZero, errorMessage) {
  return {
    'name': 'required',
    'config': {
      'error_message': errorMessage,
      'except_zero': exceptZero
    }
  }
}

ValidationUtils.TypeCheck = function (type, errorMessage) {
  return {
    'name': 'typeCheck',
    'config': {
      'type': type,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.Pattern = function (pattern, errorMessage) {
  return {
    'name': 'pattern',
    'config': {
      'pattern': pattern,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.Email = function (errorMessage) {
  return {
    'name': 'email',
    'config': {
      'error_message': errorMessage
    }
  }
}

ValidationUtils.Date = function (formats, errorMessage) {
  return {
    'name': 'date',
    'config': {
      'formats_list': typeof formats === 'string' ? [formats] : formats,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.MinimumValue = function (minimum, errorMessage) {
  return {
    'name': 'minimumValue',
    'config': {
      'min': minimum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.MaximumValue = function (maximum, errorMessage) {
  return {
    'name': 'maximumValue',
    'config': {
      'max': maximum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.Range = function (minimum, maximum, errorMessage) {
  return {
    'name': 'range',
    'config': {
      'min': minimum,
      'max': maximum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.MaximumLength = function (maximum, errorMessage) {
  return {
    'name': 'maximumLength',
    'config': {
      'max': maximum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.MinimumLength = function (minimum, errorMessage) {
  return {
    'name': 'minimumLength',
    'config': {
      'min': minimum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.RangeLength = function (minimum, maximum, errorMessage) {
  return {
    'name': 'rangeLength',
    'config': {
      'min': minimum,
      'max': maximum,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.InList = function (list, errorMessage) {
  return {
    'name': 'inList',
    'config': {
      'list': list,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.ValueCheck = function (value, errorMessage) {
  return {
    'name': 'valueCheck',
    'config': {
      'value': value,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.InList = function (list, errorMessage) {
  return {
    'name': 'inList',
    'config': {
      'list': list,
      'error_message': errorMessage
    }
  }
}

ValidationUtils.DateDiff = function (formats, cType, dType, max, min, errorMessage, secondType, secondValue, dBetween) {
  return {
    'name': 'dateDiff',
    'config': {
      'formats_list': typeof formats === 'string' ? [formats] : formats,
      'second_value_type': secondType,
      'second_value': secondValue,
      'minimum_difference': min,
      'maximum_differencee': max,
      'cheking_type': cType,
      'diff_type': dType,
      'diff_between': dBetween,
      'error_message': errorMessage
    }
  }
}

// let configuration = {
//   'return_type': 'single',
//   'configuration': {
//     'dob': {
//       'validations': [
//         ValidationUtils.Required('Please enter Date of Birth.'),
//         ValidationUtils.Date(['DD/MM/YYYY', 'MM/DD/YYYY'], 'Please enter valid Date of Birth.')
//       ]
//     }
//   }
// }

// let data = {
//   'dob': '12/24/2011'
// }
// console.log(ValidationUtils.validate(data, configuration));
// console.log(moment("12/24/2011", ["DD/MM/YYYY", "MM/DD/YYYY"], true).isValid())
