'use strict'

const multer = require('multer')
const fs = require('fs')
// const fse = require('fs-extra')
const ImagePack = require('imagemagick')
const download = require('image-downloader')
const mv = require('mv')
const XLSX = require('xlsx')
// const AppConfig = require('config')
const sharp = require('sharp')
var FileUtils = module.exports

var prepareStorage = function (uploadPath, fileName) {
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, uploadPath)
    },
    filename: function (req, file, cb) {
      console.log(req.name)
      cb(null, fileName)
    }
  })

  return storage
}

FileUtils.upload = function (request, response, uploadRootPath, uploadPath, fileName, keyName, maxSize, isUnlink, type) {
  this.exec = function (callback) {
    if (type === 'single') {
      FileUtils.uploadSingle(request, response, uploadRootPath, uploadPath, fileName, keyName, maxSize, isUnlink, callback)
    }
  }
}

FileUtils.uploadSingle = function (request, response, uploadRootPath, uploadPath, fileName, keyName, maxSize, isUnlink, callback) {
  let multerConfig = {}
  multerConfig.storage = prepareStorage(uploadPath, fileName)
  if (maxSize) {
    multerConfig.limits = {
      fileSize: maxSize
    }
  }

  console.log('uploading image')
  let upload = multer(multerConfig).single(keyName)
  // console.log("called to convert %%%%%%%%%%%%5");
  // ImagePack.convert([uploadPath + "/" + fileName, '-density', 340,  "1_converted.jpg"],
  //   function (err, stdout) {
  //     console.log("$$$$$$$$$$$$$$$$$$$$$$ file name ", uploadPath + "/" + fileName, err, stdout);
  //     if (err) {
  //       throw err;
  //     }
  //     console.log('stdout:', stdout);
  //   }
  // );

  if (isUnlink && fs.existsSync(uploadRootPath)) {
    fs.unlink(uploadRootPath, function (deleteExistedError) {
      if (deleteExistedError) {
        let error = {}
        error.code = 'DELETE_EXISTED_ERROR'
        return callback(error, fileName)
      };

      upload(request, response, function (uploadError) {
        return callback(uploadError, fileName)
      })
    })
  } else {
    upload(request, response, function (uploadError) {
      return callback(uploadError, fileName)
    })
  }
}

FileUtils.cropImage = function (source, destination, width, height, quality, gravity, options, callback) {
  gravity = gravity || 'North'
  quality = quality || 0.85
  if (!options) {
    options = {
      srcPath: source,
      dstPath: destination,
      width: width,
      height: height,
      quality: quality,
      gravity: gravity // [NorthWest | North | NorthEast | West | Center | East | SouthWest | South | SouthEast], defaults to Center.
    }
  };

  ImagePack.crop(options, function (error, stdout, stderr) {
    callback(error, stdout, stderr)
  })
}

FileUtils.downloadImage = function (url, filePath, callback) {
  const options = {
    url: url,
    dest: filePath
  }

  download.image(options)
    .then(function (fileName, image) {
      return callback(null, fileName)
    })
    .catch(function (error) {
      return callback(error, null)
    })
}

FileUtils.moveFile = function (source, destination, callback) {
  console.log(source)
  if (fs.existsSync(source)) {
    mv(source, destination, function (error) {
      return callback(error, destination)
    })
  } else {
    let callbackError = 'Destination does not exists.'
    return callback(callbackError, null)
  }
}

FileUtils.readXL = function (filePath, sheetName, sheetIndex, callback) {
  if (!fs.existsSync(filePath)) {
    let callbackError = 'Given file path does not exists.'
    return callback(callbackError)
  }

  if (!(sheetName || sheetIndex || sheetIndex === 0)) {
    console.log('sheet index error')
    let callbackError = 'Neither sheet name nor sheet index given.'
    return callback(callbackError)
  }

  let workbook = XLSX.readFile(filePath)
  let sheetNameList = workbook.SheetNames
  sheetName = sheetName || sheetNameList[sheetIndex]

  var XlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName])

  return callback(null, XlData)
}

FileUtils.resize = (path, format, width, height) => {
  const readStream = fs.createReadStream(path)
  let transform = sharp()

  if (format) {
    transform = transform.toFormat(format)
  }

  if (width || height) {
    transform = transform.resize(width, height, {
      kernel: sharp.kernel.cubic
    }).background('white').embed()
  }

  return readStream.pipe(transform)
}
