'use strict'
const SendSMSServices = require('./services/SendSMSServices')
module.exports = function (app) {
  app.get('/send-sms', SendSMSServices.sendSms)
}
