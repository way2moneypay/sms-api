// const mongoose = require('mongoose')
var SendSMSServices = module.exports
const ResponseUtils = require('./../utils/ResponseUtils')
const SmsCountsModel = require('../models/SmsCountsModel')
// const Logs = new ResponseUtils.LogsCommon()
const AppConfig = require('config')
const shortid = require('shortid')
const path = require('path')
const mongoose = require('mongoose')
const MYSQL = require("mysql");
const Request = require('request');

SendSMSServices.sendSms = function (apiRequest, apiResponse) {
    let thisFileName = './../services/SendSMSServices.js'
    let thisFunctionName = 'SendSMSServices.sendSms'
    let defaultError = 'Error while sending sms.'
    let defaultErrorCode = 'ERR-SEND-SMS'
    /* defining logs */
    let Logs = new ResponseUtils.Logs(thisFileName, thisFunctionName)
    let Print = new ResponseUtils.Print(thisFileName, thisFunctionName)
    let Response = new ResponseUtils.Response(apiResponse)
    //console.log('Body @ create link', apiRequest.query)
    try {
        var configuration = {};
        configuration.mobile = apiRequest.query.mobile;
        configuration.sender_id = apiRequest.query.sender_id;
        let body = apiRequest.query.alert_content;
        if (body && body != '') {
            if (configuration.sender_id === 'WAYMNY') {
                console.log('*******TEmplate Name at SendSMSServices.sms***************', configuration)
                if (configuration.mobile && configuration.mobile.length === 10) {
                    console.log('*******MOBILE IS VALID***************')
                    let con = MYSQL.createConnection({
                        host: AppConfig.smsdb.hostname,
                        user: AppConfig.smsdb.username,
                        password: AppConfig.smsdb.password,
                        database: AppConfig.smsdb.database
                    })
                    con.connect(function (err) {
                        if (err) {
                            console.log('__==RECHED TO FIRST ERR==___')
                            console.error(err)
                        }
                        let senderId = configuration.sender_id ? configuration.sender_id : apiRequest.query.sender_id
                        console.log('configuration.mobile :: ', configuration.mobile)
                        console.log('BODY :: ', body)
                        console.log('configuration.sender_id :: ', senderId)
                        let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
                        con.query(sql, [configuration.mobile, body, senderId, senderId], function (err, result) {
                            con.end()
                            if (err) {
                                console.log('__==RECHED TO SECOND ERR==___')
                                console.error(err)
                                return callback(err)
                            } else {
                                let date = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear()
                                SmsCountsModel.findOne({ "created_at": date }, (errSendSms, SmsData) => {
                                    if (errSendSms) {
                                        return Response.error("SMS sending Error", "Error", 500)
                                    } else if (!SmsData) {
                                        let smsCountsModel = new SmsCountsModel({
                                            sent_count: 1,
                                            created_at: date
                                        })
                                        smsCountsModel.save((errSaveShortUrl, shortUrlSaved) => {
                                            if (errSaveShortUrl) {
                                                return Response.error('Error while creating sms send data in to database', 500)
                                            } else {
                                                console.log('Saved Short URL :: ', shortUrlSaved)
                                                return Response.success('Successfully Sent SMS and Created into Database', 'SUCCESS')
                                            }
                                        })
                                    } else if (SmsData) {
                                        console.log('SmsData :: ', SmsData)
                                        SmsData.sent_count = SmsData.sent_count + 1
                                        SmsData.save((errUpdatesent_counts, Updatecount) => {
                                            if (errUpdatesent_counts) {
                                                return Response.error('Error while updating sms send count', 'ERR_UPDATE_SENTCOUNT', 500)
                                            } else {
                                                //console.log('Updatecount  :: ', Updatecount)
                                                return Response.success('SMS SEND SUCCESSFULLY', 'SUCCESS')
                                            }
                                        })
                                    } else {
                                        console.log('NO Data Found')
                                        return Response.success('No Data Found ', 'SUCCESS')
                                    }
                                })
                            }
                        })
                    });
                } else {
                    console.log('invalid mobile number length')
                    let callbackError = 'invalid mobile number'
                    return Response.error(callbackError, 'ERR')
                }
            } else {
                console.log('Your Sender Id is not WAYMNY')
                let callbackError = 'Your sender_id is not WAYMNY'
                return Response.error(callbackError, 'ERR')
            }
        } else {
            console.log('alert_content is Not present')
            let callbackError = 'alert_content  is Not present'
            return Response.error(callbackError, 'ERR')
        }
    } catch (exception) {
        Print.error(defaultError, defaultErrorCode)
        Logs.exception(exception + '', defaultErrorCode, defaultError)
        return Response.error(defaultError, defaultErrorCode, 500)
    }
}