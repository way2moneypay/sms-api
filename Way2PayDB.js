'use strict'
const Mongoose = require('mongoose').Mongoose
Mongoose.Promise = require('bluebird')
const config = require('config')
var processExit = false

const options = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 25, // Maintain up to 25 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: require('bluebird'),
  useNewUrlParser: true
}

const mongoWay2Pay = new Mongoose()

const db = mongoWay2Pay.connection

db.once('error', function (err) {
  console.error('mongoose connection error' + err)
  mongoWay2Pay.disconnect()
})
db.on('open', function () {
  console.log('successfully connected to WAY2PAY DB')
})
db.on('reconnected', function () {
  console.log('MongoDB reconnected!')
})
db.on('disconnected', function () {
  console.log('MongoDB disconnected!')
  if (!processExit) {
    mongoWay2Pay.connect(config.mongodb.Way2PayDBUri, options)
      .then(() => console.log('connection successful to WAY2PAY DB'))
      .catch((err) => console.error(err))
  }
})

// mongoose.connect(config.mongodb.uri, options);

mongoWay2Pay.connect(config.mongodb.Way2PayDBUri, options)
  .then(() => console.log('connection successful to Way2Pay DB'))
  .catch((err) => console.error(err))

exports = module.exports = mongoWay2Pay // expose app
